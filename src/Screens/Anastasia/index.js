import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import axios from 'axios';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://jsonplaceholder.typicode.com/users')
      .then(res => this.setState({data: res.data}));
  }

  render() {
      const {data} = this.state;
    return (
      <View>
          {data && data.map((value, index)=>{
              if(value.website=='anastasia.net')
              return(
                  <View key={index}>
                      <Text>ID : {value.id}</Text>
                      <Text>Name : {value.name}</Text>
                      <Text>Username : {value.username}</Text>
                      <Text>Email : {value.email}</Text>
                      <Text>Address : {value.address.street}, {value.address.suite}, {value.address.city}</Text>
                      <Text>Phone : {value.phone}</Text>
                      <Text>website : {value.website}</Text>
                  </View>
              )
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
