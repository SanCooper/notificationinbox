import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      angka: [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20,
      ],
    };
  }

  componentDidMount(){
    const {angka} = this.state;
    numArray = angka && angka.filter(number=>{
      if(number<2){
        return false;
      }
      for(let i=2;i<number;i++){
        if(number%i==0)return false;
      }
      return true
    })
    this.setState(
      {angka: numArray}
    )
  }

  render() {
    const {angka} = this.state;
    // var angkaTertinggi = Math.max.apply(null, angka);
    // numArray = angka.filter(number=>{
    //   if(number<2){
    //     return false;
    //   }
    //   for(let i=2;i<number;i++){
    //     if(number%i==0)return false;
    //   }
    //   return true
    // })
    // console.log(numArray);
    return (
      <View style={{flex: 1}}>
        {angka &&
          angka.map((value, index) => {
            return (
              <View key={index}>
                <Text>{value}</Text>
              </View>
            );
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
