import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import axios from 'axios';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://jsonplaceholder.typicode.com/comments?postId=1')
      .then(res => this.setState({data: res.data}));
  }

  render() {
    const {data} = this.state;
    return (
      <ScrollView>
        <View>
          {data &&
            data.map((value, i) => {
              return (
                <View key={i} style={{borderBottomWidth:2}}>
                  <Text>PostId : {value.postId}</Text>
                  <Text>ID : {value.id}</Text>
                  <Text>Name : {value.name}</Text>
                  <Text>Email : {value.email}</Text>
                  <Text>Body : {value.body}</Text>
                </View>
              );
            })}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({});
