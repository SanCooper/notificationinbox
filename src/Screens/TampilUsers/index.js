import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import axios from 'axios';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://jsonplaceholder.typicode.com/users')
      .then(res => this.setState({data: res.data}));
  }

  render() {
    const {data} = this.state;
    return (
      <ScrollView>
        <View>
          {data &&
            data.map((value, i) => {
              return (
                <View key={i} style={{borderBottomWidth:2}}>
                  <Text>ID : {value.id}</Text>
                  <Text>Name : {value.name}</Text>
                  <Text>Username : {value.username}</Text>
                  <Text>email : {value.email}</Text>
                </View>
              );
            })}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({});
