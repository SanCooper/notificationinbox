import {Text, StyleSheet, View, TextInput, Button} from 'react-native';
import React, {Component} from 'react';
import auth from '@react-native-firebase/auth';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      email: '',
      password: '',
    };
  }

  componentDidMount() {}

  _submit = () => {
    const {email, password} = this.state;
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        console.log('User account created & signed in!');
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.error(error);
      });
  };

  render() {
    const {data, email, password} = this.state;
    return (
      <View style={{backgroundColor: 'blue'}}>
        <Text>Sign Up</Text>
        <View>
          <TextInput
            placeholderTextColor="white"
            placeholder="Email"
            onChangeText={input => this.setState({email: input})}
          />
        </View>
        <View>
          <TextInput
            placeholderTextColor="white"
            placeholder="Password"
            onChangeText={input => this.setState({password: input})}
          />
        </View>
        <Button
          title="SUBMIT"
          onPress={() => {
            this._submit(email, password);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
