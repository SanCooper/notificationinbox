import {
  Text,
  StyleSheet,
  View,
  ActionSheetIOS,
  TextInput,
  Button,
} from 'react-native';
import React, {Component} from 'react';
import firestore from '@react-native-firebase/firestore';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      Isi: '',
      Minuman: '',
      Harga: '',
    };
  }

  componentDidMount() {
    // firestore()
    //   .collection('Menu')
    //   .doc('Paket-A')
    //   .onSnapshot(res => {
    //     this.setState({data: [res.data()]});
    //     // console.log(res);
    //   });

    firestore()
      .collection('Menu')
      .onSnapshot(value => {
        let tampungan = value.docs.map(res => {
          return res.data();
        });
        tampungan.length > 0 && this.setState({data: tampungan});
      });
  }

  _addData = () => {
    const {Isi, Minuman, Harga} = this.state;
    firestore()
      .collection('Menu')
      .doc('Paket-A')
      .set({
        Isi: Isi,
        Minuman: Minuman,
        Harga: Harga,
      })
      .then(() => {
        console.log('Data added!');
      });
    this.setState({
      Isi: '',
      Minuman: '',
      Harga: '',
    });
  };

  _delete = () => {};

  render() {
    const {data, Isi, Minuman, Harga} = this.state;
    console.log(data)
    // console.log(data);
    return (
      <View>
        <View style={styles.card}>
          <Text style={{alignSelf: 'center'}}>Menu Warteg Bersama</Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <Text style={{width: '15%', alignItems: 'center'}}>Paket</Text>
            <Text style={{width: '25%', alignItems: 'center'}}>Isi</Text>
            <Text style={{width: '20%', alignItems: 'center'}}>Minuman</Text>
            <Text style={{width: '25%', alignItems: 'center'}}>Harga</Text>
          </View>
          {data &&
            data.map((value, index) => {
              return (
                <View
                  key={index}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                  }}>
                  <Text style={{width: '15%', alignItems: 'center'}}>
                    {index + 1}
                  </Text>
                  <Text style={{width: '25%', alignItems: 'center'}}>
                    {value.Isi}
                  </Text>
                  <Text style={{width: '20%', alignItems: 'center'}}>
                    {value.Minuman}
                  </Text>
                  <Text style={{width: '25%', alignItems: 'center'}}>
                    Rp. {value.Harga},-
                  </Text>
                </View>
              );
            })}
        </View>
        <View style={styles.textInput}>
        <Text style={{alignSelf: 'center'}}>Tambah List Paket</Text>
          <TextInput
            value={Isi}
            onChangeText={input => {
              this.setState({Isi: input});
            }}
            placeholder="Isi Menu"
          />
          <TextInput
            value={Minuman}
            onChangeText={input => {
              this.setState({Minuman: input});
            }}
            placeholder="Minuman"
          />

          <TextInput
            value={Harga}
            onChangeText={input => {
              this.setState({Harga: input});
            }}
            placeholder="Harga"
          />
        <Button
          title="Ganti"
          onPress={() => {
            this._addData();
          }}
        />
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
    borderRadius: 20,
    borderWidth: 1,
    margin: 5,
  },
  textInput: {
    justifyContent: 'space-around',
    marginTop: 15,
    borderWidth: 1,
    borderRadius: 20,
    padding: 10,
    margin: 10,
    // flexDirection: 'row',
  },
});
