import {Text, StyleSheet, View, Button} from 'react-native';
import React, {Component} from 'react';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      menuList: [
        {name: 'Bilangan Ganjil', route: 'Ganjil'},
        {name: 'Bilangan Genap', route: 'Genap'},
        {name: 'Bilangan Prima', route: 'Prima'},
        {name: 'Bilangan Fibonacci', route: 'Fibonacci'},
        {name: 'Tampilkan Post', route: 'Tampil Post'},
        {name: 'Tampilkan Users', route: 'Tampil Users'},
        {name: 'Tampilkan Users anastasia.net', route: 'Tampil Users anastasia.net'},
        {name: 'Tes Firebase', route: 'Firebase'},
        {name: 'Sign Up', route: 'Sign Up'},
        {name: 'Login', route: 'Login'},
        {name: 'Implement Firestore', route: 'Implement Firestore'},
        {name: 'Inbox', route: 'Inbox'},
      ],
    };
  }
  render() {
      const {menuList} = this.state;
      const {navigation} = this.props;
    return (
      <View>
          {menuList && menuList.map((value, i)=>{
              return(
                  <View key={i} style={{marginBottom:10}}>
                      <Button title={value.name} onPress={()=> navigation.navigate(`${value.route}`)}/>
                  </View>
              )
          })}
        
      </View>
    );
  }
}

const styles = StyleSheet.create({});
