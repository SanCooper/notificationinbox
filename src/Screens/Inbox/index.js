import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
} from 'react-native';
import React, {Component} from 'react';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import AntDesign from 'react-native-vector-icons/AntDesign';
import messaging from '@react-native-firebase/messaging';
import {convertDateTime, convertTime} from '../../Components/Utils/moment';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      inbox: [],
    };
  }

  componentDidMount() {
    this._retrieveData();
    this._checkToken();
  }

  _checkToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
       console.log(fcmToken);
    } 
   }

  _retrieveData = async () => {
    try {
      const jsonValue = await AsyncStorageLib.getItem('newInboxData');
      return jsonValue != null && this.setState({inbox: JSON.parse(jsonValue)});
    } catch (error) {
      console.log(error);
    }
  };

  _deleteInbox = () => {
    Alert.alert(
      'Hapus semua?',
      'Setelah Anda menghapus semua pesan, Anda tidak dapat membatalkannya.',
      [
        {
          text: 'HAPUS',
          onPress: () => this._clearAllData(),
        },
        {text: 'Batal', onPress: () => console.log('Batal Pressed')},
      ],
    );
  };

  _clearAllData = () => {
    AsyncStorageLib.getAllKeys().then(keys =>
      AsyncStorageLib.multiRemove(keys),
    );
    this.setState({inbox: []});
  };

  render() {
    const {inbox} = this.state;
    const {navigation} = this.props;
    console.log(inbox);
    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={styles.header}>
          <View>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <AntDesign name="close" size={30} style={{color: 'black'}} />
            </TouchableOpacity>
          </View>
          <View>
            <Text style={{fontSize: 25, fontWeight: 'bold', color: 'black'}}>
              Pesan
            </Text>
          </View>
          <View>
            <TouchableOpacity onPress={() => this._deleteInbox()}>
              <AntDesign name="delete" size={30} style={{color: 'black'}} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          {inbox.length >= 1 ? (
            inbox.map((value, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPress={() => navigation.navigate('Detail Message', value)}
                  style={styles.inboxCard}>
                  <View>
                    <Text style={styles.inboxTitle}>{value.title}</Text>
                    <Text style={styles.inboxBody}>
                      {value.body.substr(0, 100)}
                    </Text>
                    <Text style={styles.inboxDate}>
                      {convertDateTime(new Date(value.sentTime))}
                    </Text>
                  </View>
                  <View>
                    <AntDesign name="right" size={30} />
                  </View>
                </TouchableOpacity>
              );
            })
          ) : (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                paddingTop: '50%',
              }}>
              <Text>Tidak ada pesan.</Text>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    padding: 10,
  },
  inboxCard: {
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    // marginRight: 20,
    // marginLeft:20,
    paddingRight: 20,
    paddingVertical: 12,
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderBottomColor: 'grey',
  },
  inboxTitle: {
    fontSize: 19,
    fontWeight: 'bold',
    color: 'black',
  },
  inboxBody: {
    fontSize: 15,
    paddingTop: 5,
    color: 'black',
  },
  inboxDate: {
    fontSize: 13,
    paddingTop: 30,
  },
});
