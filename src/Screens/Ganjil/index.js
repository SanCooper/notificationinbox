import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      angka: [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20,
      ],
    };
  }
  render() {
    const {angka} = this.state;

    return (
      <View>
        {angka &&
          angka.map((value, i) => {
            return (
                <View key={i}>
                  <Text>{value % 2 != 0 && value}</Text>
                </View>
            );
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
