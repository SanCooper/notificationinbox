import {Text, StyleSheet, View} from 'react-native';
import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import {Ganjil, Genap, bilFibonacci, bilPrima, TampilPost, Home, TampilUsers,Anastasia, Firebase, signUp, Login, Notif, ImplementFs, Inbox, DetailMsg} from '../navigation/screen';

const Stack = createNativeStackNavigator();
function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Ganjil" component={Ganjil} />
        <Stack.Screen name="Genap" component={Genap} />
        <Stack.Screen name="Prima" component={bilPrima} />
        <Stack.Screen name="Fibonacci" component={bilFibonacci} />
        <Stack.Screen name="Tampil Post" component={TampilPost} />
        <Stack.Screen name="Tampil Users" component={TampilUsers} />
        <Stack.Screen name="Tampil Users anastasia.net" component={Anastasia} />
        <Stack.Screen name="Firebase" component={Firebase} />
        <Stack.Screen name="Sign Up" component={signUp} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Implement Firestore" component={ImplementFs} />
        <Stack.Screen name="Inbox" component={Inbox} options={{headerShown: false}}/>
        <Stack.Screen name="Detail Message" component={DetailMsg} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default index;
