import Ganjil from '../Screens/Ganjil';
import Genap from '../Screens/Genap';
import bilPrima from '../Screens/bilPrima';
import bilFibonacci from '../Screens/bilFibonacci'
import TampilPost from '../Screens/TampilPost';
import Home from '../Screens/Home';
import TampilUsers from '../Screens/TampilUsers';
import Anastasia from '../Screens/Anastasia';
import Firebase from '../Screens/Firebase';
import signUp from '../Screens/signUp';
import Login from '../Screens/Login';
import ImplementFs from '../Screens/ImplementFs';
import Inbox from '../Screens/Inbox';
import DetailMsg from '../Screens/DetailMsg';

export {Ganjil, Genap, bilFibonacci, bilPrima, TampilPost, Home, TampilUsers,Anastasia, Firebase, signUp, Login, ImplementFs, Inbox, DetailMsg}